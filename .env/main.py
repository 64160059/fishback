# main.py

from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
import joblib
import os

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5173"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Load the trained model
file_path = '/path/to/fish_weight_prediction_model.pkl'
if os.path.exists(file_path):
    print(f'The file {file_path} exists.')
    
else:
    print(f'The file {file_path} does not exist.')

    
model = joblib.load('fish_weight_prediction_model.pkl')
class FishMeasurement(BaseModel):
    Species: int
    Length1: float
    Length2: float
    Length3: float
    Height: float
    Width: float

class FishWeigth(BaseModel):
    Weight: float

@app.post("/fish/predict")
async def predict_fish_weight(fish: FishMeasurement):
    try:
        # Use the FishMeasurment object to predict the weight
        data = [[fish.Species, fish.Length1, fish.Length2, fish.Length3, fish.Height, fish.Width]]
        prediction = model.predict(data)

        # Create a FishWeight object to return the predicted weight
        predict_weight = FishWeigth(Weight=prediction[0])

        return predict_weight
    except Exception as e:
        print("Error prediction:", e)
        return {"error": "Prediction failed"}
